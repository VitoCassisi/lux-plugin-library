Lux Plug-in Library  
=  

###NOTE!
This library is still in beta, and is subject to change. The current release of Lux does not utilise the entire API available here.

What's all this then?
-
**Lux** was designed to work on all Android devices by utilising the official Android API. This API was never designed to allow system tools to exist, let alone an auto brightness replacement, which meant that Lux has always relied on overlays and other workarounds to get the job done. As root access on devices becomes increasingly mainstream, it is obvious that these limitations can be bypassed. Unfortunately, the superuser commands for adjusting brightness, display RGB, etc. differ between devices, making it difficult to keep up with new devices and their various kernels/ROMs.

The **Lux Plug-in Library** allows *anybody* to extend Lux's functionality to include **low-level display adjustment**.

What benefits does this provide?
-
When the plug-in library is fully implemented for a given device, Lux will automagically become capable of the following upon install:

- Low level RGB adjustment for night/astronomer/subzero
- Low level brightness adjustment
- Ability to read in sensor values directly from the kernel

Additionally, many issues related to the use of overlays disappear. Screenshots are no longer dark, and you don't need to disable Lux when secure apps are in the foreground.

Amazing! How do I make a plug-in for my device?!
-
With ease! Simply import the **Lux Plug-in Library** source into your IDE as an Android Library Project, and then create a **new** Android app which extends PassiveDisplay.java.

PassiveDisplay provides hooks into Lux's plug-in framework, which you can use to provide superuser commands for Lux to execute. These commands correlate to functions such as setting RGB values. Take a look at the **[sample Nexus 4 project](https://bitbucket.org/VitoCassisi/nexus-4-plug-in-for-lux/src)** to see how it's all done.  

Finally, in your new project's AndroidManifest.xml file, add the following:

        <activity
            android:name="com.vitocassisi.lux.plugin.MainActivity"
            android:label="@string/app_name" >
            <intent-filter>
                <action android:name="android.intent.action.MAIN" />

                <category android:name="android.intent.category.LAUNCHER" />
            </intent-filter>
        </activity>
        
        <receiver
            android:name="com.vitocassisi.lux.plugin.LuxBroadcastReceiver"
            android:exported="true" >
            <intent-filter>
                <action android:name="com.vitocassisi.lux.plugin.search" />
            </intent-filter>
        </receiver>

        <service
            android:name=".MY_PLUGIN"
            android:exported="true" >
            <intent-filter>
                <action android:name="YOUR_PACKAGE_NAME_HERE" />
            </intent-filter>
        </service>
        
Be sure to replace **YOUR_PACKAGE_NAME_HERE** with the name of your plug-in's package, and **MY_PLUGIN** with your extended PassiveDisplay class.


And that's it! Now you're ready to test!

Enabling Plug-In support within Lux
-
To enable plug-in support, you must check 'Enable plug-in support' in the Advanced section of Lux settings. Enabling this option forfeits all responsibility of mine for any issues caused while using Lux. Plug-in's can cause very real damage to your device if they're written incorrectly - don't use plug-ins from sources you don't trust!

Testing your plug-in
-
Testing is **critical** for creating a good plug-in. You are dealing directly with system files, and can easily cause damage if you do not know what you're doing.

### DO NOT RELEASE A PLUG-IN WITHOUT TESTING IT ON THE TARGET DEVICE FIRST!###

Ensure that the following work with your plug-in installed:

- Astronomer Mode
- Night Mode
- General brightness adjustment
- Lux readings functional

###BE METICULOUS WITH YOUR IMPLEMENTATION OF `isSupportedDevice()`!###

Modifying system files on the wrong model/kernel may cause irreversible issues!

Licence
-
    Copyright © 2013 Vito Cassisi
    
    You may not use this library except in compliance with the following:
        - It cannot be used for commercial purposes under any circumstances, unless explicit permission is granted in writing from the author.
        - Modifications to this library may not be distributed.
    
    Unless required by applicable law or agreed to in writing, software
    distributed under this licence is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
